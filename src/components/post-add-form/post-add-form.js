import React from 'react'

import './post-add-form.scss'

const PostAddForm = () => {
    let state = "Any ideas?"
    return (
        <form className="bottom-panel flex">
            <div className="pos-relative">
                <input
                    className="balloon" 
                    id="idea" 
                    type="text" 
                    placeholder="Here we go!"
                    required
                />
                <label htmlFor="idea">{state}</label>
            </div>
            <button
                type="submit"
                className="btn-green btn-main"
            >
                Add
            </button>
        </form>
    )
}

export default PostAddForm