import React from 'react';
import './app-header.scss'

const AppHeader = (props) => {
    let state = "Mr. "
    let name = "Andy"
    let surname = "Cortez"

    return (
        <div className="app-header flex">
            <div className="logo-test">
                <h1>HERCULES</h1>
            </div>
            <div className="avatar-icon flex">
                <img src="/images/man.svg" className="star" />
                <h1>{state} {props.name} {props.surname} Guest</h1>
            </div>
            <h2>23 posts, liked 0</h2>
        </div>
    )
}

export default (AppHeader)