import React from 'react'
import './search-filter.scss'

const SearchFilter = () => {
    let state = "What's up?"
    return (
        <div className="pos-relative">
            <form>
                <input
                className="balloon" 
                id="state" 
                type="text" 
                placeholder="Write your emotions!"
                required
                />
                <label htmlFor="state">{state}</label>
                <button 
                    className="btn-green btn-main"
                    type="submit"
                >Add your state</button>
            </form>
        </div>
    )
}

export default SearchFilter