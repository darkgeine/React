import React, {Component} from 'react'

import './post-list-item.scss'

export default class PostListItem extends Component {
    constructor(props) {
        super(props)
        this.state ={
            important: false,
            like: false
        }
        this.onImportant = this.onImportant.bind(this)
        this.onLike = this.onLike.bind(this)
    }

    onImportant() {
        this.setState(({important}) => ({
            important: !important
        })) 
    }

    onLike() {
        this.setState(({like}) => ({
            like: !like
        })) 
    }

    render() {
        const {label} = this.props
        const {important, like} = this.state
        let classNames = "app-list-item flex justify-btw"
        if (important) {
            classNames +=" important"
        }
        if (like) {
            classNames +=" like"
        }
        return (
            <div className={classNames}>
                <span 
                    className="app-list-item-label"
                    onClick={this.onLike}
                >
                    {label}
                </span>
                <div className="flex justify-center align-center">
                    <button 
                        type="button" 
                        className="like"
                        onClick={this.onImportant}
                    >
                        <img src="/images/like.svg" />
                    </button>
                    <button 
                        type="button" 
                        className="dislike"
                    >
                        <img src="/images/dislike.svg" />
                    </button>
                    <img className="heart" src="/images/heart.svg" />
                </div>
            </div>
        )
    }
}