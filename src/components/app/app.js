import React from 'react'

import AppHeader from '../app-header'
import PostStatusFilter from '../post-status-filter'
import PostList from '../post-list'
import PostAddForm from '../post-add-form'
import SearchFilter from '../search-filter'

import './app.scss'

const App = () => {

    const data = [
        {label: 'Goint to learn React' , important: true, id: 'sds'},
        {label: 'Went for new skills' , important: false, id: 'dwdw'},
        {label: 'Road to the dream' , important: false, id: 'dsdsd'}
    ]

    return (
        <div className="app">
            <AppHeader />
            <SearchFilter />
            <div className="search-panel">
                <PostStatusFilter />
            </div>
            <PostList posts={data} />
            <PostAddForm />
        </div>
    )
}

export default App;